package ru.zolov.tm.service;

import java.util.List;
import javax.transaction.Transactional;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.entity.Project;

@Service
@Transactional
public class ProjectService extends AbstractService {

  @Autowired IProjectRepository projectRepository;

  @SneakyThrows public @NotNull Project create(
      @Nullable final Project project
  ) {
    if (project == null) throw new Exception();
    return projectRepository.save(project);
  }

  @NotNull public List<Project> findAll() {
    return (List<Project>)projectRepository.findAll();
  }

  public void update(
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description
  ) {
    if (id == null || id.isEmpty()) return;
    if (name == null || name.isEmpty()) return;
    if (description == null || description.isEmpty()) return;
    @NotNull final Project project = new Project();
    project.setId(id);
    project.setName(name);
    project.setDescription(description);
    projectRepository.save(project);
  }

  public Project findById(@NotNull final String id) {
    if (projectRepository.findById(id).isPresent()) {
      return projectRepository.findById(id).get();
    }
    return null;
  }

  @SneakyThrows public void removeById(
      @Nullable final String id
  ) {
    if (id == null || id.isEmpty()) throw new Exception();
    @Nullable final Project project = projectRepository.findById(id).orElseThrow(Exception::new);
    projectRepository.delete(project);
  }

  public @NotNull List<Project> findProject(
      @Nullable final String part
  ) {
    if (part == null || part.isEmpty()) return (List<Project>)projectRepository.findAll();
    @NotNull List<Project> list = projectRepository.findProjectsByNameContainsOrDescriptionContains(part, part);
    return list;
  }
}
