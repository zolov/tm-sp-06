package ru.zolov.tm.service;

import java.text.SimpleDateFormat;
import java.util.Locale;

public abstract class AbstractService {

  public SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
}
