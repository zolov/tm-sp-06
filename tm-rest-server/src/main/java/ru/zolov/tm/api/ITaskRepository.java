package ru.zolov.tm.api;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.zolov.tm.entity.Task;

@Repository
public interface ITaskRepository extends CrudRepository<Task, String> {

  @Query("SELECT t FROM Task t WHERE t.id = :id")
  Optional<Task> findOneById(@NotNull @Param("id") String id);

  @Query("SELECT t FROM Task t WHERE t.projectId = :projectId")
  List <Task> findAllTasksByProjectId(@NotNull @Param("projectId") String projectId);

  @Transactional
  @Modifying
  @Query("DELETE FROM Task t WHERE t.projectId = :projectId")
  void deleteAllByProjectId(@NotNull @Param("projectId") String projectId);

  @Query("SELECT t FROM Task t WHERE (t.name LIKE CONCAT ('%',:string,'%') OR t.description LIKE CONCAT ('%',:string,'%'))")
  List<Task> findTaskByPathOfTheName(@Param("string") @NotNull String string);
}
