package ru.zolov.tm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractGoal {

  @Column(name = "project_id")
  @NotNull private String projectId = "";
}
