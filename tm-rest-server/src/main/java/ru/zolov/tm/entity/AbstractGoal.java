package ru.zolov.tm.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.constant.StatusType;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractGoal extends AbstractEntity {

  @Column(name = "name")
  @NotNull
  private String name = "";
  @Column(name = "date_create")
  @NotNull
  private Date dateOfCreate = new Date();
  @Column(name = "date_start")
  @NotNull
  private Date dateOfStart;
  @Column(name = "date_finish")
  @NotNull
  private Date dateOfFinish;
  @Column(name = "description")
  @NotNull
  private String description = "empty";
  @Column(name = "status")
  @NotNull
  @Enumerated(EnumType.STRING)
  private StatusType status = StatusType.PLANNED;
}
