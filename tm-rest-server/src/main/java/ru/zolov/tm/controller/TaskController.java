package ru.zolov.tm.controller;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;

@RestController
public class TaskController {

  @Autowired TaskService taskService;
  @Autowired ProjectService projectService;

  @GetMapping("/projects/tasks/{projectId}") public List<Task> getTaskList(@PathVariable("projectId") final String projectId) {
    List<Task> tasks = taskService.findTaskByProjectId(projectId);
    return tasks;
  }

  @PostMapping(value = "/projects/tasks") public RedirectView createTask(
      @RequestBody final Task task
  ) {
    taskService.create(task);
    return new RedirectView("/tasks/" + task.getProjectId());
  }

  @PutMapping(value = "/projects/tasks") public RedirectView editTask(
      @RequestParam(name = "id") final String id,
      @RequestParam(name = "name") String name,
      @RequestParam(name = "description") String description
  ) {
    @NotNull final Task task = taskService.findTaskById(id);
    return new RedirectView("/projects");
  }

  @DeleteMapping(value = "/projects/tasks/{id}") public RedirectView deleteTask(@PathVariable("id") String id) {
    taskService.removeTaskById(id);
    return new RedirectView("/projects");
  }
}
