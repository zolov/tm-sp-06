package ru.zolov.tm.service;

import java.util.Arrays;
import java.util.List;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.zolov.tm.entity.Project;


@Service
public class ProjectService extends AbstractService {

  private static String URL = "http://localhost:8080/projects";

  @SneakyThrows public @NotNull Project create(
      @Nullable final String name,
      @Nullable final String description
  ) {
    RestTemplate template = new RestTemplate();
    Project project = new Project();
    project.setName(name);
    project.setDescription(description);
    return template.postForObject(URL, project, Project.class);
  }

  public List<Project> findAll() {
    RestTemplate template = new RestTemplate();
    final Project[] projects = template.getForObject(URL, Project[].class);
    return Arrays.asList(projects);
  }

  public void update(
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description
  ) {
    final RestTemplate template = new RestTemplate();
    final Project project = template.getForObject(URL + "/" + id, Project.class);
    project.setId(id);
    project.setName(name);
    project.setDescription(description);
    template.put(URL, project, id);
  }

  @SneakyThrows public void removeById(
      @Nullable final String id
  ) {
    final RestTemplate template = new RestTemplate();
    template.delete(URL + "/" + id);
  }

  public @Nullable Project findById(@Nullable String id) {
    final RestTemplate template = new RestTemplate();
    return template.getForObject(URL + "/" + id, Project.class);
  }

  public @NotNull List<Project> findProject(
      @Nullable final String part
  ) {
    RestTemplate template = new RestTemplate();
    final Project[] projects = template.getForObject(URL, Project[].class);
    return Arrays.asList(projects);
  }
}
