package ru.zolov.tm.service;

import java.util.Arrays;
import java.util.List;
import javax.transaction.Transactional;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Task;

@Service
@Transactional
public class TaskService extends AbstractService {

  private static String URL = "http://localhost:8080/projects/tasks";

  @SneakyThrows public @NotNull Task create(
      @Nullable final String projectId,
      @Nullable final String name
  ) {
    final RestTemplate template = new RestTemplate();
    Task task = new Task();
    task.setProjectId(projectId);
    task.setName(name);
    return template.postForObject(URL, task, Task.class);
  }

  @SneakyThrows public List<Task> findTaskByProjectId(
      @Nullable final String id
  ) {
    final RestTemplate template = new RestTemplate();
    final Task[] tasks = template.getForObject(URL + "/" + id, Task[].class);
    return Arrays.asList(tasks);
  }

  @SneakyThrows public @Nullable Task findTaskById(
      @Nullable final String id
  ) {
    final RestTemplate template = new RestTemplate();
    return template.getForObject(URL + "/" + id, Task.class);
  }

  @SneakyThrows public void removeTaskById(
      @Nullable final String id
  ) {
    final RestTemplate template = new RestTemplate();
    template.delete(URL + "/" + id);
  }

  @SneakyThrows public void update(
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description
  ) {
    final RestTemplate template = new RestTemplate();
    final Task task = template.getForObject(URL + "/" + id, Task.class);
    task.setId(id);
    task.setName(name);
    task.setDescription(description);
  }

  public @NotNull List<Task> findTask(
      @Nullable final String partOfTheName
  ) throws Exception {
    final RestTemplate template = new RestTemplate();
    final Task[] tasks = template.getForObject(URL, Task[].class);
    return Arrays.asList(tasks);
  }
}
