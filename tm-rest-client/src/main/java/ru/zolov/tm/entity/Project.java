package ru.zolov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.constant.StatusType;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project {
  @NotNull private String id = UUID.randomUUID().toString();
  @NotNull private String name = "Demo";
  @NotNull private String description = "empty";
  @NotNull private Date dateOfCreate = new Date();
  @NotNull private Date dateOfStart = new Date();
  @NotNull private Date dateOfFinish = new Date();
  @NotNull private StatusType status = StatusType.PLANNED;
}
