package ru.zolov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.zolov.tm.constant.StatusType;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task {

  private String id = UUID.randomUUID().toString();
  private String projectId;
  private String name;
  private Date dateOfCreate;
  private Date dateOfStart;
  private Date dateOfFinish;
  private String description;
  private StatusType status;
}
