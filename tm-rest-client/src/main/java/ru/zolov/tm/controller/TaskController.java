package ru.zolov.tm.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;

@Controller
public class TaskController {

  @Autowired TaskService taskService;
  @Autowired ProjectService projectService;

  @GetMapping("/tasks/{projectId}") public ModelAndView getTasks(@PathVariable("projectId") final String projectId) {
    List<Task> tasks = taskService.findTaskByProjectId(projectId);
    ModelAndView model = new ModelAndView();
    model.addObject("tasks", tasks);
    model.addObject("projectId", projectId);
    model.setViewName("task-list");
    return model;
  }

  @GetMapping(value = "/task-create/{projectId}") public ModelAndView createTaskGet(
      @PathVariable(name = "projectId") String projectId
  ) {
    ModelAndView model = new ModelAndView();
    model.addObject("projectId", projectId);
    model.setViewName("create-task");
    return model;
  }

  @PostMapping(value = "/task-create/{projectId}") public ModelAndView createTaskPost(
      @PathVariable(name = "projectId") @ModelAttribute("projectId") String projectId,
      @ModelAttribute("name") String name,
      @ModelAttribute("description") String description
  ) {
    ModelAndView model = new ModelAndView();
    Project project = projectService.findById(projectId);
    taskService.create(project.getId(), name);

    model.setViewName("redirect:/tasks/" + projectId);
    return model;
  }

  @GetMapping(value = "/task-edit-get/{id}") public ModelAndView editProjectGet(@PathVariable("id") final String id) {
    ModelAndView model = new ModelAndView();
    Task task = taskService.findTaskById(id);
    model.addObject("task", task);
    model.addObject("projectId" , task.getProjectId());
    model.setViewName("edit-task");
    return model;
  }

  @PostMapping(value = "task-edit-post/{id}") public ModelAndView editProjectPost(@ModelAttribute("task") final Task task
  ) {
    ModelAndView model = new ModelAndView();
    taskService.update(task.getId(), task.getName(), task.getDescription());
    model.setViewName("redirect:/projects");
    return model;
  }

  @GetMapping(value = "/task-delete/{id}") public ModelAndView deleteTask(@PathVariable("id") String id) {
    ModelAndView model = new ModelAndView();
    taskService.removeTaskById(id);
    model.setViewName("redirect:/projects");
    return model;
  }

}
