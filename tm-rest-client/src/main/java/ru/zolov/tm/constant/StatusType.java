package ru.zolov.tm.constant;

public enum StatusType {
  PLANNED,
  INPROGRESS,
  DONE;
}
