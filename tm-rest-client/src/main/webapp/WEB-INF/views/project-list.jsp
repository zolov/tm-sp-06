<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: zolov
  Date: 11.02.2020
  Time: 18:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <title>Task Manager</title>
</head>

<body>
<%@ include file="topbar.jsp" %>
<div class="container-fluid">
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Project ID</th>
        <th scope="col">Project name</th>
        <th scope="col">Status</th>
        <th scope="col">Description</th>
        <th scope="col">Created</th>
        <th scope="col">Begin</th>
        <th scope="col">End</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${projects}" var="project">
        <tr>
            <td>${project.id}</td>
            <td>${project.name}</td>
            <td>${project.status}</td>
            <td>${project.description}</td>
            <td>${project.dateOfCreate}</td>
            <td>${project.dateOfStart}</td>
            <td>${project.dateOfFinish}</td>
            <td>
            <td>
                <div class="btn-group" role="group" aria-label="actions">
                    <a class="btn btn-primary btn-sm" href="/edit/${project.id}">edit</a>
                    <a class="btn btn-primary btn-sm" href="/delete/${project.id}">delete</a>
                    <a class="btn btn-primary btn-sm" href="/tasks/${project.id}">tasks</a>
                </div>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
    <form action="create">
        <button class="btn btn-primary btn-lg btn-block" type="submit"><b>New project</b></button>
    </form>
</div>
</body>
</html>