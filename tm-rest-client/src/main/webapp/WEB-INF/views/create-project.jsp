<%--delete/116a5621-2bad-48f7-83b1-45df407f605e
  Created by IntelliJ IDEA.
  User: zolov
  Date: 12.02.2020
  Time: 13:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <title>Task Manager</title>
</head>
<body>
<%@ include file="topbar.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm">
            <form action="/create" method="POST">
                <input type="hidden" name="id" value="${project.id}">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input class="form-control" placeholder="Enter name" type="text" name="name"
                           id="name">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input class="form-control" placeholder="Enter description" type="text"
                           name="description"
                           id="description">
                </div>
                <button class="btn btn-primary" type="submit" value="Add Project">Add Project
                </button>
            </form>
        </div>
        <div class="col-sm">
        </div>
        <div class="col-sm">
        </div>
    </div>
</div>
</body>
</html>
